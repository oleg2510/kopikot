import { Component, OnInit } from '@angular/core';
import { SiteService } from './shared/site.service';
import { AppConfig } from '../config/app.config';


/**
 * Main component of extension
 */
@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit {
  private items = [];
  private total = 0;

  constructor(private siteService: SiteService) { }

  ngOnInit() {
    this.loadPageItems();
  }

  /**
   * Load sites
   */
  loadPageItems () {
    this.total = this.total + AppConfig.topItemsLimit;
    this.siteService.getItems(this.total, 0)
      .subscribe(data => this.items = data.items);
  }
}
