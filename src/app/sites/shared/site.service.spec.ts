import {async, TestBed} from '@angular/core/testing';
import {APP_BASE_HREF} from '@angular/common';
import {APP_CONFIG, AppConfig} from '../../config/app.config';

import {SiteService} from './site.service';

describe('SiteService', () => {
  let siteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: APP_CONFIG, useValue: AppConfig},
        {provide: APP_BASE_HREF, useValue: '/'},
        SiteService
      ]
    });
    siteService = TestBed.get(SiteService);
  });

  /**
   *  We try to count first 10 items
   *  TODO: Don't use HTTP service
   */
  it('should contains sites', async(() => {
    siteService.getItems().subscribe((data: any) => {
      expect(data.length).toBeGreaterThan(9);
    });
  }));
});
