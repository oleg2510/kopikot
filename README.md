# Kopikot chrome extension

## Build
Run `npm i`

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
This artifact you should load as unpacked extension on [extension page](chrome://extensions/) (dev mode)

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma%-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Docker

You can build the image and run the container with Docker. The configuration is in the nginx folder if you want to change it.

`docker build -t kopikot .`

`docker run -d -p 4200:80 kopikot`

## Travis CI

We use Travis CI to run this tasks in order:
* Linter
* Tests
* Build for production
* Deploy in Github pages

